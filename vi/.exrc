set nu

set showmode
# Setting showmode has the editor tell me what input mode I am in.

set nomesg
# Setting nomesg prevents incoming msgs so I don't get interrupted.
# This resets to previous setting when you exit the editor.

set hardtabs=4
# Setting hardtabs to 4 means when I hit TAB key, my indentation is 4.

set tabstops=4
# Setting tabstops is similar to hardtabs.

set report=3
# Setting report=3 means any change of 3 or more lines is reported to me.

set autoindent
# Setting autoindent means that next line of input indents to same level
# as the current line. This is good for structured programming.

#set readonly
# Setting this defaults program mode to Read Only. I can still write to
# the file but I must use the :w! command and then :q separately.
# This prevents accidentally writing to the file when I didn't want to.

set shiftwidth=4
# Setting this tells me how many spaces back I go when doing a backtab.

set nowrapscan
# Setting this tells editor to stop all searches at the end of the file
# instead of wrapping around to the beginning of file.