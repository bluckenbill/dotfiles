#!/usr/bin/bash

[[ ! -d "~/dotfiles" ]] && echo "Could not find ~/dotfiles directory" && exit 1
mkdir -p ~/.dotfiles

echo "Installing the Dracula Theme for VIM"
[[ -d "~/.vim/pack/themes/start" ]] || echo "Unable to find directory ~/.vim" && exit 1

mkir -p ~/.vim/pack/themes/start
git clone https://github.com/dracula/vim.git ~/.vim/pack/themes/start/dracula


echo "Setting up dircolors..."
git clone https://github.com/dracula/dircolors.git ~/.dotfiles/dircolors