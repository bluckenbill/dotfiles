Requires Jetbrains Mono Nerdfont: https://github.com/ryanoasis/nerd-fonts/releases/download/v2.3.3/JetBrainsMono.zip


usage:

cd ~


git clone --recursive ... 


cd dotfiles


stow ...
