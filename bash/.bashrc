# ~/.bashrc

if [[ ${EUID} == 0 ]] ; then
  PS1='\[\033[01;31m\][\h\[\033[01;36m\] \W\[\033[01;31m\]]\$\[\033[00m\] '
else
  PS1='\[\033[01;32m\][\u@\h\[\033[01;37m\] \W\[\033[01;32m\]]\$\[\033[00m\] '
fi

DIRCOLORS_FILE="~/dotfiles/dircolors/.dircolors"
test -r ${DIRCOLORS_FILE} && eval "$(dircolors ${DIRCOLORS_FILE})"

alias ls='ls --color'
