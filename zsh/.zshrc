export HISTFILESIZE=1000000000
export HISTSIZE=1000000000
setopt INC_APPEND_HISTORY appendhistory autocd beep notify
setopt HIST_FIND_NO_DUPS
setopt HIST_IGNORE_ALL_DUPS


PROMPT="%F{blue}%~ %(?.%F{green}.%F{red})%#%f "

DIRCOLORS_FILE="~/dotfiles/dircolors/.dircolors"
test -r ${DIRCOLORS_FILE} && eval "$(dircolors ${DIRCOLORS_FILE})"

alias ls='ls --color'